import QtQuick 2.0

Image {
    id: picBtn

    signal clicked()

    antialiasing: true
    fillMode: Image.PreserveAspectFit

    opacity: marea.containsMouse ? 0.5 : 1

    MouseArea {
        id: marea
        anchors.fill: parent
        hoverEnabled: true
        cursorShape: Qt.PointingHandCursor

        onClicked: picBtn.clicked()
    }
}
