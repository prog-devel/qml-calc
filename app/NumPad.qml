import QtQuick 2.0
import QtQuick.Layouts 1.1
import "NumPadMap.js" as NPMap


GridLayout {
    id: numPad

    property alias buttons: btnRepeater
    signal triggered(var id)

    columns: 5
    rows: 4

    columnSpacing: 4
    rowSpacing: 4

    focus: true

    Repeater {
        id: btnRepeater
        model: NPMap.buttons
        delegate: Button {
            text: modelData.label

            pressed: npCtl.pressedId === index

            enabled: true

            Layout.rowSpan: modelData.meta.rowSpan ? modelData.meta.rowSpan : 1
            Layout.columnSpan: modelData.meta.colSpan ? modelData.meta.colSpan : 1

            Layout.fillWidth: true
            Layout.fillHeight: true

            // NOTE: Qt propogates only own properties of JS objects (no prototypes here)
            // Qt uses QObjects with properties for signalling (no JS objects)
            onTriggered: numPad.triggered(modelData.id);
        }
    }


    QtObject {
        id: npCtl
        property int pressedId: -1
    }

    Keys.onPressed: {
        npCtl.pressedId = NPMap.getSafeIdByKey(event.key);
    }

    Keys.onReleased: {
        if (NPMap.getSafeIdByKey(event.key) === npCtl.pressedId) {
            npCtl.pressedId = -1;
        }
    }
}
