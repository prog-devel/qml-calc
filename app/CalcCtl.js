.import "NumPadMap.js" as NPMap
.import prog.devel 1.0 as P

var log = P.Processor.log;
var process = P.Processor.process;

function Token(value, type, internalValue) {
    this.value = value;
    this.type = type;
    this.internalValue = internalValue;
}

// NODE: V4 Engine do not support for classes
// TODO: `extend' function (copy own properties couple with Object.create)
Token.prototype.toString = function() { return this.value; }
Token.prototype.lastSym = function() { return this.value.length ? this.value[this.value.length - 1] : ""; }
Token.prototype.firstSym = function() { return this.value.length ? this.value[0] : ""; }
Token.prototype.popLastSym = function() { this.value = this.value.slice(0, -1); }
Token.prototype.popFirstSym = function() { this.value = this.value.slice(1); }
Token.prototype.pushLastSym = function(sym) { this.value += sym; }
Token.prototype.pushFirstSym = function(sym) { this.value = sym + this.value; }

Token.prototype.isZeroOnly = function() { return this.value === "0" || this.value === "-0"; }
Token.prototype.endsWithDot = function() { return this.lastSym() === "."; }

Token.Number = P.Processor.Number;
Token.Operator = P.Processor.Operator;

var dispValue; /// display value
var token; /// current token
var tokens; /// tokens stack

function btnItemByValue(val) {
    return txt in NPMap.maps.byValues ? btnItemById(NPMap.maps.byValues[val].id) : {};
}

function btnItemById(id) {
    return id !== undefined ? numPad.buttons.itemAt(id) : {};
}


function updateView() {
    console.log("MainCtl, token stack", token, tokens);

    disp.value = dispValue + token.toString();

    NPMap.buttons.forEach(function(btnData) {
        var item = btnItemById(btnData.id);

        if (btnData.role === NPMap.Role.BinOperator) {
            item.enabled = token.type === Token.Number;
        }
        else if (btnData.getValue() === ".") {
            item.enabled = token.type !== Token.Number || !token.hasFract || token.lastSym() === ".";
        }
        else if (btnData.getValue() === "0") {
            item.enabled = token.type !== Token.Number || !token.isZeroOnly();
        }
        else if (btnData.getValue() === "C" || btnData.getValue() === "⌫") {
            item.enabled = tokens.length || token.value !== "0";
        }
    });
}

function reset() {
    dispValue = "";
    token = new Token("0", Token.Number);
    tokens = [];
    updateView();
}

function pushNumber() {
    // remove trailing "."
    if (token.endsWithDot())
        token.popLastSym();
    dispValue += token.value;
    tokens.push(token);
}

function onTriggered(id) {
    var btn = NPMap.buttons[id];
    if (!btn) {
        console.warn("Invalid button ID", id);
        return;
    }

    // controls
    if (btn.getValue() === 'C') {
        return reset();
    }
    else if (btn.getValue() === '⌫') {
        if (token.type === Token.Number) {
            if (token.endsWithDot())
                token.hasFract = false;
            else if (token.value.length === 2 && token.firstSym() === "-")
                token.value = "0";
        }

        token.popLastSym();
        if (!token.value.length) {
            if (tokens.length) {
                token = tokens[tokens.length - 1];
                tokens.pop();
                dispValue = dispValue.slice(0, -token.value.length);
            }
            else
                return reset();
        }
    }
    else if (btn.getValue() === "=") {
        if (token.type === Token.Number) {
            pushNumber();
        }
        log(dispValue, P.Processor.Query);
        process(tokens);
        return reset();
    }
    // Numbers
    else if (btn.role === NPMap.Role.Number) {
        if (token.type !== Token.Number) {
            dispValue += token.toString();
            tokens.push(token);
            token = new Token("0", Token.Number);
        }

        var nval = btn.getValue();

        if (nval === ".") {
            // toggle trailing "."
            if (token.endsWithDot()) {
                token.popLastSym();
                token.hasFract = false;
            }
            else if (!token.hasFract) {
                token.pushLastSym(nval);
                token.hasFract = true;
            }
        }
        else if (nval === "±") {
            // toggle -/+
            if (token.firstSym() !== "-")
                token.pushFirstSym("-");
            else
                token.popFirstSym();
        }
        else {
            // remove heading "0"
            if (token.isZeroOnly())
                token.popLastSym();

            // append new digit
            token.pushLastSym(nval);
        }
    }
    // operators
    else if (btn.role === NPMap.Role.BinOperator) {
        if (token.type === Token.Number) {
            pushNumber();
            token = new Token(btn.label, Token.Operator, btn.getValue());
        }
    }
    else {
        log("Not implemented yet `" + btn.getValue() + "'", P.Processor.Error);
        return;
    }

    updateView();
}
