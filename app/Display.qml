import QtQuick 2.0

Text {
    property string placeholder
    property string value

    text: value || placeholder
    wrapMode: Text.WrapAtWordBoundaryOrAnywhere

    width: parent.width

    color: '#ffffff'
    font {family: "Mono"}
}
