import QtQuick 2.0
import QtQuick.Layouts 1.1

Item {
    id: btn

    property alias text: btnText.text
    property bool pressed

    // TODO use Qt styles
    property var style: QtObject {
        property color bgNormal: "#eceeea"
        property color bgChecked: Qt.darker(bgNormal)
        property color bgHover: Qt.lighter(bgNormal)
        property color bgDisabled: "#ddeceeea"

        property color fgNormal: Qt.darker(bgNormal)
        property color fgChecked: Qt.lighter(fgNormal)
        property color fgDisabled: "#aaeceeea"
    }


    signal triggered()

    onPressedChanged: {
        if (enabled && pressed) {
            triggered();
        }
    }

    QtObject {
        id: p
        property bool checked: enabled && (btnMouse.pressed || pressed)
    }

    Rectangle {
        id: btnBg

        anchors.fill: parent
        color: bgColor()
    }

    Text {
        id: btnText
        horizontalAlignment: Text.AlignHCenter
        verticalAlignment: Text.AlignVCenter
        width: parent.width
        height: parent.height
        font {pixelSize: (parent.height < parent.width ? parent.height : parent.width) / 2; family: "Mono"}
        color: fgColor()
        font.bold: true
    }

    MouseArea {
        id: btnMouse
        anchors.fill: parent
        hoverEnabled: true
        onPressed: triggered()

        cursorShape: Qt.PointingHandCursor
    }

    function bgColor() {
       return enabled
        ? (p.checked
           ? style.bgChecked
           : (!btnMouse.containsMouse
              ? style.bgNormal
              : style.bgHover))
        : style.bgDisabled;
    }

    function fgColor() {
       return enabled
        ? (p.checked
           ? style.fgChecked
           : style.fgNormal)
        : style.fgDisabled;
    }
}

