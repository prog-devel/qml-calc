#include "processor.h"

#include <QtQml>
#include <QDebug>
#include <QtConcurrent>

using namespace Calc;

static QString errorStr(int err) {
    switch (err) {
    case ErrNone: return QT_TR_NOOP("Success");
    case ErrInvOperator: return QT_TR_NOOP("Invalid operator");
    case ErrInvOperand: return QT_TR_NOOP("Invalid operand");
    default: return QT_TR_NOOP("Unknown error");
    }
}

Processor::Token::Token(const QJSValue& jtok)
    : value(jtok.property("value").toVariant())
    , internalValue(jtok.property("internalValue").toVariant())
    , type(jtok.property("type").toInt())
{}

Processor::Processor(QQmlEngine* engine, QObject *parent)
    : QObject(parent)
    , _qmlEng(engine)
{
    connect(&_requests, SIGNAL(sizeChanged(int)), this, SIGNAL(reqSizeChanged(int)));
    connect(&_results, SIGNAL(sizeChanged(int)), this, SIGNAL(resSizeChanged(int)));

    _workers << QtConcurrent::run([this] {
        while (!_stop) {
            auto ret = _requests.dequeue();
            if (!ret.first) continue;

            QThread::msleep(_delay * 1000);

            try {
                _results.enqueue({"", calcExpr(std::move(ret.second))});
            }
            catch (const QString& what) {
                _results.enqueue({what, 0});
            }
        }
    });

    _workers << QtConcurrent::run([this] {
        while (!_stop) {
            auto ret = _results.dequeue();
            if (!ret.first) continue;
            const auto& res = ret.second;

            QThread::msleep(_delayResp * 1000);

            if (!res.first.length()) {
                log(QString::number(res.second), Info);
            }
            else {
                log(res.first, Error);
            }
        }
    });
}

Processor::~Processor()
{
    _stop = 1;

    _requests.wake();
    _results.wake();

    for (auto& w : _workers)
        w.waitForFinished();
}

void Processor::process(const QJSValue& jtoks)
{
    if (!jtoks.isArray()) {
        log("Invalid tokens: `" + jtoks.toString() + "'", Error);
        return;
    }

    RequestType tokens;

    auto len = jtoks.property("length").toInt();
    for (int i = 0; i < len; ++i) {
        auto jtok = jtoks.property(i);
        tokens.push_back(Token(jtok));
    }
    _requests.enqueue(tokens);
}

void Processor::log(const QString& msg, int type)
{
    // XXX it's thread safe for auto-connection
    emit logged(msg, type);
}


Processor::Tokens Processor::toRPN(Tokens toks)
{
    static QMap<int, int> PRIORITY_TABLE = {
        {Add, 0},
        {Sub, 0},
        {Mul, 1},
        {Div, 1}
    };

    QStack<Token> stack;
    QQueue<Token> outq;

    for (const auto& tok : toks) {
        if (tok.type == Number) {
            outq << tok;
        }
        else if (tok.type == Operator) {
            if (!PRIORITY_TABLE.contains(tok.value.toInt())) {
                throw tr("Invalid priority for token: `%1'").arg(tok.value.toInt());
            }

            while (!stack.empty() &&
                   PRIORITY_TABLE[tok.value.toInt()] <= PRIORITY_TABLE[stack.top().value.toInt()]) {
                outq << stack.pop();
            }

            stack << std::move(tok);
        }
        else {
           throw tr("Invalid token type: `%1' (type=%2)").arg(tok.value.toString()).arg(tok.type);
        }
    }

    while (!stack.empty()) {
        outq << stack.pop();
    }

    return outq;
}

double Processor::calcExpr(Tokens toks)
{
    auto outq = toRPN(std::move(toks));

    QStack<double> regs;
    bool ok;

    for (const auto& tok : outq) {
        if (tok.type == Number) {
            auto value = tok.value.toDouble(&ok);
            if (!ok) {
                throw tr("Invalid number: `%1'").arg(tok.value.toString());
            }
            regs << value;
        }
        else if (tok.type == Operator) {
            auto b = regs.pop();
            auto& a = regs.top();

            int err;
            a = doIt(tok.internalValue.toInt(), a, b, err);
            if (err != ErrNone) {
                throw errorStr(err) + tr(" (op=`%1', a=`%2', b=`%3')")
                        .arg(tok.value.toString())
                        .arg(a)
                        .arg(b);
            }
        }
        else { /* NOT REACHED */ }
    }

    return regs.front();
}
