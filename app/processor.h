#ifndef PROCESSOR_H
#define PROCESSOR_H

#include <QObject>
#include <QList>
#include <QJSValue>

#include <QFuture>
#include <QAtomicInt>

#include "calc.h"
#include "threadedqueue.h"

class QQmlEngine;

namespace Calc {

class Processor : public QObject
{
    Q_OBJECT

    Q_ENUMS(LogMessageType TokenType Operator)

    Q_PROPERTY(qreal delay READ delay WRITE setDelay NOTIFY delayChanged)
    Q_PROPERTY(qreal delayResp READ delayResp WRITE setDelayResp NOTIFY delayRespChanged)

    Q_PROPERTY(int reqSize READ reqSize NOTIFY reqSizeChanged)
    Q_PROPERTY(int resSize READ resSize NOTIFY resSizeChanged)

public:
    enum TokenType {
        Number,
        Operator
    };

    // XXX meta-enums must be declared inside QObject-ed class
    // but we use these values in external library
    enum Operator {
        Add = Calc::Add,
        Sub = Calc::Sub,
        Mul = Calc::Mul,
        Div = Calc::Div
    };

    enum LogMessageType {
        Query,
        Info,
        Error
    };

    struct Token {
        Token(const QJSValue& jtok);
        Token() = default;

        QVariant value;
        QVariant internalValue;
        int type{Number};
    };

    using Tokens = QList<Token>;
    using RequestType = Tokens;
    using ResultType = QPair<QString, double>;

    explicit Processor(QQmlEngine* engine, QObject *parent = 0);
    ~Processor();

    Q_INVOKABLE void process(const QJSValue& jtoks);

    qreal delay() const { return _delay; }
    qreal delayResp() const { return _delayResp; }

    int reqSize() const { return _requests.size(); }
    int resSize() const { return _results.size(); }

signals:
    // thread safe log
    void logged(const QString& msg, int type);

    void delayChanged(qreal);
    void delayRespChanged(qreal);

    void reqSizeChanged(int size);
    void resSizeChanged(int size);

public slots:
    void log(const QString& msg, int type);

    void setDelay(qreal delay) { _delay = delay; }
    void setDelayResp(qreal delay) { _delayResp = delay; }

protected:
    //! converts invfix notation to reverse Polish notation
    static Tokens toRPN(Tokens toks);

    //! evaluates expression in infix notation
    static double calcExpr(Tokens toks);

private:
    QQmlEngine* _qmlEng;

    qreal _delay{0};
    qreal _delayResp{0};

    QList<QFuture<void>> _workers;
    QAtomicInt _stop{0};

    ThreadedQueue<RequestType> _requests;
    ThreadedQueue<ResultType> _results;
};

}

#endif // PROCESSOR_H
