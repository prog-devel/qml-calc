import QtQuick 2.0

import prog.devel 1.0
import "utils.js" 1.0 as U

Item {
    property alias text: log.text

    function push(msg, type) {
        log.text = U.fmtLog(msg, type) + log.text
    }

    function clear() {
        log.text = "";
    }

    clip: true

    Flickable {
        id: flick

        anchors.fill: parent

        contentWidth: log.paintedWidth
        contentHeight: log.paintedHeight

        flickableDirection: Flickable.VerticalFlick

        clip: true

        Text {
            id: log

            width: flick.width
            height: flick.height

            wrapMode: Text.WrapAnywhere
            font.pixelSize: 16
            textFormat: Text.StyledText
        }
    }

    Rectangle {
        id: scrollbar
        anchors.right: flick.right
        y: flick.visibleArea.yPosition * flick.height
        width: 10
        height: flick.visibleArea.heightRatio * flick.height
        visible: flick.visibleArea.heightRatio < 1
        color: Qt.darker("#ffffff")
    }
}

