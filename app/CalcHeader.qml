import QtQuick 2.5
import QtQuick.Layouts 1.1

Item {
    property bool checked: true

    Display {
        anchors.fill: parent
        font.pixelSize: 20
        fontSizeMode: Text.VerticalFit
        maximumLineCount: 1
        horizontalAlignment: Text.AlignLeft
        verticalAlignment: Text.AlignVCenter
        text: qsTr("Luke I'm your father...")
    }

    PicButton {
        source: "settings.png"

        anchors.top: parent.top
        anchors.bottom: parent.bottom
        anchors.right: parent.right

        onClicked: checked = !checked
    }

    HorizontalLine {
        anchors.top: parent.bottom
        anchors.left: parent.left
        anchors.right: parent.right
    }
}
