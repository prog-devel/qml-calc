#ifndef THREADEDQUEUE
#define THREADEDQUEUE

#include <QObject>
#include <QPair>
#include <QQueue>

#include <QMutex>
#include <QWaitCondition>

#include <utility>

class ThreadedQueueMeta: public QObject
{
    Q_OBJECT
public:
    virtual ~ThreadedQueueMeta() {}

signals:
    void sizeChanged(int);
};


template <class T>
class ThreadedQueue final: public ThreadedQueueMeta
{
public:
    // type traits
    using QueueType = QQueue<T>;
    using DequeueType = QPair<bool, T>;
    using size_type = typename QueueType::size_type;

    ~ThreadedQueue() { }

    DequeueType dequeue() {
        QMutexLocker lock(&_m);
        while (_q.empty() && _cond.wait(&_m, 100)) { }
        if (_q.empty())
            return DequeueType{false, T{}};

        auto ret = DequeueType{true, _q.dequeue()};
        emit sizeChanged(size());
        return ret;
    }

    void enqueue(const T& obj) {
        QMutexLocker lock(&_m);
        _q.enqueue(obj);
        emit sizeChanged(size());
        _cond.wakeAll();
    }

    inline size_type size() const { return _q.count(); }

    inline void wake() { _cond.wakeAll(); }

private:
    QueueType _q;
    QMutex _m;
    QWaitCondition _cond;
};

#endif // THREADEDQUEUE

