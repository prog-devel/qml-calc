import QtQuick 2.5
import QtQuick.Layouts 1.1
import QtQuick.Controls 1.2 as QtCtl

import prog.devel 1.0

ColumnLayout {
    id: ctlPanel

    property alias delay: delaySlider.value
    property alias delayResp: delaySliderResp.value

    property alias log: log

    Text {
        text: "Req delay: " + delaySlider.value.toFixed(2) + " sec"
        color: "#ffffff"
        font.pixelSize: 20
    }

    QtCtl.Slider {
        id: delaySlider

        minimumValue: 0
        maximumValue: 5
        stepSize: 0.01
    }

    Text {
        text: "Resp delay: " + delaySliderResp.value.toFixed(2) + " sec"
        color: "#ffffff"
        font.pixelSize: 20
    }

    QtCtl.Slider {
        id: delaySliderResp

        minimumValue: 0
        maximumValue: 5
        stepSize: 0.01
    }

    RowLayout {
        Text {
            id: logLabel
            text: "Log"
            color: "#ffffff"
            font.pixelSize: 20
        }

        PicButton {
            source: "clear.png"
            Layout.preferredHeight: logLabel.contentHeight
            onClicked: log.clear()
        }

        Text {
            text: qsTr("ReqN: ") + Processor.reqSize.toString()
            color: "#ffffff"
            font.pixelSize: 14
        }

        Text {
            text: qsTr("ResN: ") + Processor.resSize.toString()
            color: "#ffffff"
            font.pixelSize: 14
        }
    }

    HorizontalLine { Layout.fillWidth: true }

    LogView {
        id: log
        Layout.fillWidth: true
        Layout.fillHeight: true
    }

    HorizontalLine { Layout.fillWidth: true }

    Binding {
        target: Processor
        property: "delay"
        value: delaySlider.value
    }

    Binding {
        target: Processor
        property: "delayResp"
        value: delaySliderResp.value
    }

    Connections {
        target: Processor
        onLogged: log.push(msg, type)
    }

}
