.pragma library

.import prog.devel 1.0 as P

var logColorMap = {};
var logPrefixMap = {};

logColorMap[P.Processor.Query] = "lightBlue";
logColorMap[P.Processor.Info] = "lightGreen";
logColorMap[P.Processor.Error] = "red";

logPrefixMap[P.Processor.Query] = "⇒ ";
logPrefixMap[P.Processor.Info] = "= ";
logPrefixMap[P.Processor.Error] = "⚠ ";

function fmtLog(msg, type) {
    return "<font color='"
            + (type in logColorMap ? logColorMap[type] : "white")
            + "'> "
            + (type in logPrefixMap ? logPrefixMap[type] : "")
            + msg + "</font><br/>"
}
