/// make this script shared with each other in same scope
/// NOTE: a shared script (i.e., defined as .pragma library) does not inherit imports
/// from any QML document even if it imports no other scripts or modules
/// so DO NOT try access to QML-instances from it
.pragma library

.import prog.devel 1.0 as P


var Role = {
    BinOperator: 1,
    Number: 2,
    Control: 3,
}

var buttons = [
    {label: '⌫',
     role: Role.Control,
     key: Qt.Key_Backspace
    },
    {label: 'C',
     role: Role.Control,
     keys: [Qt.Key_Escape, Qt.Key_Delete]
    },
    {label: '(',
     keys: [Qt.Key_ParenLeft, Qt.Key_BraceLeft, Qt.Key_BracketLeft]
    },
    {label: ')',
     keys: [Qt.Key_ParenRight, Qt.Key_BraceRight, Qt.Key_BracketRight]
    },
    {label: '√'},
    {label: '7',
     role: Role.Number,
     key: Qt.Key_7
    },
    {label: '8',
     role: Role.Number,
     key: Qt.Key_8
    },
    {label: '9',
     role: Role.Number,
     key: Qt.Key_9
    },
    {label: '÷',
     value: P.Processor.Div,
     role: Role.BinOperator,
     key: Qt.Key_Slash
    },
    {label: '%',
     key: Qt.Key_Percent
    },
    {label: '4',
     role: Role.Number,
     key: Qt.Key_4
    },
    {label: '5',
     role: Role.Number,
     key: Qt.Key_5
    },
    {label: '6',
     role: Role.Number,
     key: Qt.Key_6
    },
    {label: '×',
     value: P.Processor.Mul,
     role: Role.BinOperator,
     key: Qt.Key_Asterisk
    },
    {label: '1/x'},
    {label: '1',
     role: Role.Number,
     key: Qt.Key_1
    },
    {label: '2',
     role: Role.Number,
     key: Qt.Key_2
    },
    {label: '3',
     role: Role.Number,
     key: Qt.Key_3
    },
    {label: '−',
     value: P.Processor.Sub,
     role: Role.BinOperator,
     key: Qt.Key_Minus
    },
    {label: '=',
     keys: [Qt.Key_Equal, Qt.Key_Enter, Qt.Key_Return],
     meta: { rowSpan: 2 }
    },
    {label: '0',
     role: Role.Number,
     key: Qt.Key_0
    },
    {label: '±',
     role: Role.Number,
    },
    {label: ',',
     value: '.',
     role: Role.Number,
     keys: [Qt.Key_Period, Qt.Key_Comma]
    },
    {label: '+',
     value: P.Processor.Add,
     role: Role.BinOperator,
     key: Qt.Key_Plus
    }
];

var ButtonProto = {
    getValue: function() { return this.value || this.label; }
}

buttons.forEach(function(btn, id) {
    btn.id = id;
    if (!btn.meta) btn.meta = {}; // access safety
    btn.__proto__ = ButtonProto;
})


var maps = {
    byKeys: buttons.reduce(function(acc, val) {
        if (val.key !== undefined)
            acc[val.key] = val;
        else if (val.keys !== undefined)
            val.keys.forEach(function(k) {
                acc[k] = val;
            });
        return acc;
    }, {}),

    byValues: buttons.reduce(function(acc, btn) {
        acc[btn.getValue()] = btn;
        return acc;
    }, {})
};

function getSafeBtnByValue(val) {
    return val in maps.byValues ? maps.byValues[val] : {};
}

function getSafeIdByKey(key) {
    return key in maps.byKeys ? maps.byKeys[key].id : -1;
}
