#include <QtQml>
#include <QtWidgets/QApplication>

#include "processor.h"

// TODO accel

using namespace Calc;

int main(int argc, char *argv[])
{
    QApplication::setOrganizationName(QStringLiteral("prog.devel"));
    QApplication::setApplicationName(QStringLiteral("YetAnotherCalc"));

    QApplication app(argc, argv);

    qmlRegisterSingletonType<Processor>(
                "prog.devel", 1, 0, "Processor",
                [] (QQmlEngine *engine, QJSEngine *scriptEngine) -> QObject*
    {
        Q_UNUSED(engine)
        Q_UNUSED(scriptEngine)
        return new Processor(engine);
    });

    QQmlApplicationEngine engine;
    engine.load(QUrl(QStringLiteral("qrc:///main.qml")));

    return app.exec();
}

