import QtQuick 2.5
import QtQuick.Controls 1.2 as QtCtl
import Qt.labs.settings 1.0
import QtQuick.Layouts 1.1

QtCtl.ApplicationWindow {
    id: main

    title: 'Yet another calc'
    visible: true

    width: 480
    height: 480

    color: Qt.lighter("#272822")

    // Quit app on Ctrl+Q
    QtCtl.Action {
        shortcut: StandardKey.Quit
        onTriggered: Qt.quit()
    }

    // Control panel on Ctrl+Tab
    QtCtl.Action {
        id: actTriggerPanel
        shortcut: StandardKey.NextChild
        onTriggered: calcHdr.checked = !calcHdr.checked
    }

    // Reset log on Ctrl+R
    QtCtl.Action {
        shortcut: StandardKey.Refresh
        onTriggered: ctlPanel.log.clear()
    }

    // Bind geometry to settings %conf_path%/prog.devel/YetAnotherCalc.conf
    Settings {
        id: mainCfg
        property alias x: main.x
        property alias y: main.y
        property alias width: main.width
        property alias height: main.height
        property alias controlsVisible: calcHdr.checked
        property alias delay: ctlPanel.delay
        property alias delayResp: ctlPanel.delayResp
    }

    RowLayout {
        id: docker
        anchors.fill: parent

        ColumnLayout {
            Layout.minimumWidth: 200

            CalcHeader {
                id: calcHdr
                Layout.fillWidth: true
                Layout.preferredHeight: 35
            }

            Calc {
                id: calc
                Layout.margins: 4
                Layout.fillWidth: true
                Layout.fillHeight: true
            }
        }

        ControlPanel {
            id: ctlPanel

            Layout.alignment: Qt.AlignTop;
            Layout.maximumWidth: calcHdr.checked ? main.width / 3 : 0
            Layout.margins: 10

            visible: Layout.maximumWidth > 0

            Behavior on Layout.maximumWidth { PropertyAnimation {duration: 100} }
        }

    }
}

