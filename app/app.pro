TEMPLATE = app

QT += qml quick widgets concurrent
CONFIG += c++14

OBJECTS_DIR = ../.build
MOC_DIR = ../.build
DESTDIR = ../out

SOURCES += main.cpp \
    processor.cpp \
    threadedqueue.cpp

RESOURCES += qml.qrc

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Default rules for deployment.
include(deployment.pri)

INCLUDEPATH += ../lib

HEADERS += \
    processor.h \
    threadedqueue.h

LIBS += -lcalc -L../out
