import QtQuick 2.5
import QtQuick.Layouts 1.1

import "CalcCtl.js" as Ctl

Rectangle {
    color: "#272822"

    ColumnLayout {
        spacing: 4
        anchors.fill: parent

        TextMetrics {
            id: dispMetrics
            font.family: disp.font.family
            font.pixelSize: parent.height / 8
            text: "0"
        }

        Display {
            id: disp

            Layout.preferredHeight: dispMetrics.height * maximumLineCount
            Layout.fillWidth: true

            font: dispMetrics.font
            maximumLineCount: 2

            horizontalAlignment: Text.AlignRight
        }

        NumPad {
            id: numPad
            Layout.fillHeight: true

            onTriggered: Ctl.onTriggered(id)
        }

        Component.onCompleted: Ctl.reset()
    }
}
