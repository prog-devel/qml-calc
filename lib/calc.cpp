#include "calc.h"

double Calc::doIt(int op, double a, double b, int& errCode)
{
    errCode = ErrNone;

    switch (op) {
    case Add: return a + b;
    case Sub: return a - b;
    case Mul: return a * b;
    case Div: {
        if (b == 0) {
            errCode = ErrInvOperand;
            return 0;
        }
        return a / b;
    }
    default:
        errCode = ErrInvOperator;
        return 0;
    }
}
