#pragma once

#include <QtCore/QtGlobal>

#if defined(LIBCALC)
#  define LIBCALC_EXPORT Q_DECL_EXPORT
#else
#  define LIBCALC_EXPORT Q_DECL_IMPORT
#endif

namespace Calc {

enum Operator {
    Add,
    Sub,
    Mul,
    Div
};

enum ErrorCode {
    ErrNone = 0,
    ErrInvOperand,
    ErrInvOperator
};

LIBCALC_EXPORT double doIt(int op, double a, double b, int& errCode);

}
