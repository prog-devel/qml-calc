TEMPLATE = lib

TARGET = calc
CONFIG += sharedlib

OBJECTS_DIR = ../.build
MOC_DIR = ../.build
DESTDIR = ../out

HEADERS = calc.h
SOURCES = calc.cpp

DEFINES += LIBCALC
